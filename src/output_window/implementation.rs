use gtk::glib;
use gtk::subclass::prelude::*;

#[derive(Default)]
pub struct OutputWindow {
    pub workspaces:
        std::cell::RefCell<std::collections::HashMap<String, crate::workspace_page::WorkspacePage>>,
}

#[glib::object_subclass]
impl ObjectSubclass for OutputWindow {
    type ParentType = gtk::ApplicationWindow;
    type Type = super::OutputWindow;

    const NAME: &'static str = "I3TreeVisualOutputWindowOutputWindow";
}

impl ApplicationWindowImpl for OutputWindow {}

impl ObjectImpl for OutputWindow {}

impl WidgetImpl for OutputWindow {}

impl WindowImpl for OutputWindow {}
