use color_eyre::eyre::{self, ContextCompat};
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

use crate::display_container::DisplayContainerisable;

mod implementation;

glib::wrapper! {
    pub struct OutputWindow(ObjectSubclass<implementation::OutputWindow>)
        @extends gtk::ApplicationWindow, gtk::Window, gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl OutputWindow {
    pub fn new(application: &crate::tree_visual::TreeVisual, title: &str) -> eyre::Result<Self> {
        log::debug!("Creating new output with title: {}", title);

        let notebook = gtk::Notebook::builder()
            .enable_popup(true)
            .scrollable(true)
            .build();

        let object = glib::Object::new(&[
            ("application", application),
            ("child", &notebook),
            ("title", &title),
            ("default-height", &200 as &dyn ToValue),
            ("default-width", &300 as &dyn ToValue),
        ])?;

        Ok(object)
    }

    pub fn update(&self, output: &i3_ipc::reply::Node) -> eyre::Result<()> {
        log::debug!("Updating output: {}", output.display());

        let notebook = self
            .child()
            .expect("Output windows should have a child attached when they're created.")
            .downcast::<gtk::Notebook>()
            .expect("Output windows should always have a `gtk::Notebook` as a child.");
        let mut workspaces = self.imp().workspaces.borrow_mut();
        let mut new_workspaces = std::collections::HashMap::new();

        let content_id = output.focus[0];
        let workspaces_node = output
            .nodes
            .iter()
            .find(|node| node.id == content_id)
            .wrap_err_with(|| {
                format!(
                    "Focused child could not be found: Child {} Parent {}",
                    content_id,
                    output.display(),
                )
            })?;

        log::trace!("Found workspace container: {}", workspaces_node.display(),);

        let focused_id = workspaces_node.focus[0];
        for node in &workspaces_node.nodes {
            let name = node.name.clone().wrap_err_with(|| {
                format!("Name expected on workspace container: {}", node.display(),)
            })?;

            let workspace = if let Some(workspace) = workspaces.remove(&name) {
                workspace
            } else {
                let workspace = crate::workspace_page::WorkspacePage::new()?;
                // TODO: I don't like calling `.imp().drawing_area` everywhere...
                notebook.append_page(
                    &workspace.imp().drawing_area,
                    Some(&gtk::Label::new(Some(&name))),
                );
                workspace
            };

            let index = notebook.page_num(&workspace.imp().drawing_area);

            workspace.update(node);

            new_workspaces.insert(name, workspace);

            if node.id == focused_id {
                log::trace!("Focusing tab: Index {:?} Id {}", index, node.id);
                notebook.set_current_page(index);
            }
        }

        for page in workspaces.values() {
            let index = notebook.page_num(&page.imp().drawing_area);
            log::info!(
                "Removing tab for non-existent workspace: {}",
                page.imp().containers.borrow().display(),
            );
            notebook.remove_page(index);
        }

        *workspaces = new_workspaces;

        Ok(())
    }
}
