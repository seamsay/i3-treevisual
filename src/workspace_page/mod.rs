use color_eyre::eyre;
use gtk::glib;
use gtk::subclass::prelude::*;
use gtk::traits::WidgetExt;

use crate::display_container::DisplayContainerisable;

mod implementation;

glib::wrapper! {
    pub struct WorkspacePage(ObjectSubclass<implementation::WorkspacePage>)
        @extends gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl WorkspacePage {
    pub fn new() -> eyre::Result<Self> {
        let object = glib::Object::new(&[])?;
        Ok(object)
    }

    pub fn update(&self, workspace: &i3_ipc::reply::Node) {
        log::debug!("Updating workspace: {}", workspace.display(),);

        let implementation = self.imp();

        let containers = implementation.containers.clone();
        let mut containers = containers.borrow_mut();
        *containers = workspace.into();

        implementation.drawing_area.queue_draw();
    }
}
