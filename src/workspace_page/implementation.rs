use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use reingold_tilford::NodeInfo;

use crate::display_container::DisplayContainerisable;

#[derive(Clone, Copy, Debug)]
pub enum Layout {
    Unknown,
    Horizontal,
    Stacking,
    Tabbed,
    Vertical,
}

impl std::default::Default for Layout {
    fn default() -> Self {
        Layout::Unknown
    }
}

impl From<i3_ipc::reply::NodeLayout> for Layout {
    fn from(layout: i3_ipc::reply::NodeLayout) -> Self {
        use i3_ipc::reply::NodeLayout::*;
        use Layout::*;

        match layout {
            SplitH => Horizontal,
            SplitV => Vertical,
            Stacked => Stacking,
            i3_ipc::reply::NodeLayout::Tabbed => Layout::Tabbed,
            _ => Unknown,
        }
    }
}

impl From<Layout> for crate::display_container::Layout {
    fn from(layout: Layout) -> Self {
        use crate::display_container::Layout::*;
        match layout {
            Layout::Unknown => Unknown,
            Layout::Horizontal => Horizontal,
            Layout::Stacking => Stacking,
            Layout::Tabbed => Tabbed,
            Layout::Vertical => Vertical,
        }
    }
}

#[derive(Debug, Default)]
pub struct Container {
    children: Vec<Self>,
    focused: bool,
    id: usize,
    layout: Layout,
    name: Option<String>,
    window: Option<u32>,
}

impl<'n, 'c: 'n> From<&'c Container> for crate::display_container::DisplayContainer<'n> {
    fn from(container: &'c Container) -> Self {
        crate::display_container::DisplayContainer {
            focused: container.focused,
            id: container.id,
            layout: container.layout.into(),
            name: container.name.as_ref().map(|s| &s[..]),
            window: container.window,
        }
    }
}

impl Container {
    fn with_focus(node: &i3_ipc::reply::Node, focus: bool) -> Self {
        Container {
            children: node
                .nodes
                .iter()
                .map(|node| Container::with_focus(node, focus | node.focused))
                .collect(),
            focused: focus,
            id: node.id,
            layout: node.layout.into(),
            name: node.name.clone(),
            window: node.window,
        }
    }
}

impl From<&i3_ipc::reply::Node> for Container {
    fn from(node: &i3_ipc::reply::Node) -> Self {
        Self::with_focus(node, node.focused)
    }
}

impl<'c> DisplayContainerisable<'c> for Container {
    fn display<'d>(&'c self) -> crate::display_container::DisplayContainer<'d>
    where
        'c: 'd,
    {
        crate::display_container::DisplayContainer::from(self)
    }
}

impl std::fmt::Display for Container {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.display())
    }
}

struct Tree;

impl<'c> NodeInfo<&'c Container> for Tree {
    type Key = usize;

    fn key(&self, node: &'c Container) -> Self::Key {
        node.id
    }

    fn children(&self, node: &'c Container) -> reingold_tilford::SmallVec<&'c Container> {
        node.children.iter().collect()
    }

    fn dimensions(&self, _node: &'c Container) -> reingold_tilford::Dimensions {
        reingold_tilford::Dimensions {
            top: 0.03,
            right: 0.1,
            bottom: 0.03,
            left: 0.1,
        }
    }

    fn border(&self, _node: &'c Container) -> reingold_tilford::Dimensions {
        reingold_tilford::Dimensions::all(0.02)
    }
}

impl Container {
    fn bounding_box(
        &self,
        layout: &std::collections::HashMap<usize, reingold_tilford::Coordinate>,
    ) -> reingold_tilford::Dimensions {
        let mut min_x = std::f64::INFINITY;
        let mut max_x = std::f64::NEG_INFINITY;
        let mut min_y = std::f64::INFINITY;
        let mut max_y = std::f64::NEG_INFINITY;

        let mut stack = vec![self];
        while let Some(node) = stack.pop() {
            stack.extend(node.children.iter());

            let coordinate = layout[&node.id];
            min_x = min_x.min(coordinate.x - Tree.dimensions(node).left - Tree.border(node).left);
            max_x = max_x.max(coordinate.x + Tree.dimensions(node).right + Tree.border(node).right);
            min_y = min_y.min(coordinate.y - Tree.dimensions(node).top - Tree.border(node).top);
            max_y =
                max_y.max(coordinate.y + Tree.dimensions(node).bottom + Tree.border(node).bottom);
        }

        let r#box = reingold_tilford::Dimensions {
            left: min_x,
            right: max_x,
            top: min_y,
            bottom: max_y,
        };

        log::debug!("Calculated bounding box: {:?}", r#box);

        r#box
    }

    fn draw_function(
        &self,
        _area: &gtk::DrawingArea,
        cairo: &gtk::cairo::Context,
        width: f64,
        height: f64,
    ) {
        log::debug!(
            "Drawing workspace: {} Width {} Height {}",
            self,
            width,
            height
        );

        // TODO: The following two checks are needed because i3's `for_window` command
        // doesn't seem to pick up this window.

        const DEFAULT_WIDTH: f64 = 1.0;
        let width = if width <= 0.0 {
            log::warn!(
                "Drawing area width is `{}`, setting to `{}`.",
                width,
                DEFAULT_WIDTH
            );

            DEFAULT_WIDTH
        } else {
            width
        };

        const DEFAULT_HEIGHT: f64 = DEFAULT_WIDTH;
        let height = if height <= 0.0 {
            log::warn!(
                "Drawing area height is `{}`, setting to `{}`.",
                height,
                DEFAULT_HEIGHT
            );

            DEFAULT_HEIGHT
        } else {
            height
        };

        let layout = reingold_tilford::layout(&Tree, self);
        let bounding_box = self.bounding_box(&layout);

        let scale_x = width / (bounding_box.right - bounding_box.left);
        let scale_y = height / (bounding_box.bottom - bounding_box.top);
        log::debug!("Calculated scale: {} x {}", scale_x, scale_y);

        cairo.scale(scale_x, scale_y);

        cairo.set_source_rgb(0.95, 0.95, 0.95);
        cairo
            .paint()
            .expect("TODO: Can I bubble Cairo errors up sensibly?");

        cairo.set_line_width(0.001);

        let mut stack = vec![self];
        while let Some(node) = stack.pop() {
            log::trace!("Drawing container: {}", node);

            let coordinate = layout[&node.id];
            let dimensions = Tree.dimensions(node);
            log::trace!(
                "Container layout and dimensions: {:?} {:?}",
                coordinate,
                dimensions
            );

            let reingold_tilford::Coordinate { x, y } = coordinate;
            let reingold_tilford::Dimensions {
                top,
                bottom,
                left,
                right,
            } = dimensions;

            let box_height = top + bottom;
            let box_width = left + right;
            let box_top = y - top;
            let box_left = x - left;
            let box_bottom = y + bottom;

            // TODO: Put the window id after the container id, and the container name on a
            // third line. For the case of the window id, the font size should
            // be adjusted until the line fits in the box. For the case of the
            // container name, the name should be truncated until it fits in the box.
            // More generally, the font size of the line should be adjusted down to no less
            // than 75% of it's intended height and truncated otherwise (maybe a mix of the
            // two, i.e. truncate then see if reducing the size can make it fit then
            // truncate again if not).
            let lines = [format!("{:?}", node.layout), format!("{}", node.id)];

            let line_height = box_height / (lines.len() as f64);
            // NOTE: We don't want the lines to be pressed up against each other, so we reduce
            // their size slightly.
            const TEXT_HEIGHT_FACTOR: f64 = 0.9;
            let text_height = line_height * TEXT_HEIGHT_FACTOR;
            log::trace!("Text height: {:?}", text_height);

            cairo.set_source_rgb(0.25, 0.25, 0.25);
            cairo.select_font_face(
                "Sans",
                gtk::cairo::FontSlant::Normal,
                gtk::cairo::FontWeight::Normal,
            );
            cairo.set_font_size(text_height);

            for (i, line) in lines.iter().enumerate() {
                cairo.move_to(box_left, box_top + (i as f64) * line_height + text_height);
                cairo
                    .show_text(line)
                    .expect("TODO: Can I bubble Cairo errors up sensibly?");
            }

            if node.focused {
                cairo.set_source_rgb(1.0, 0.0, 0.0);
            }

            cairo.rectangle(box_left, box_top, box_width, box_height);

            let meet_point = y + bottom + Tree.border(node).bottom;

            for child in Tree.children(node) {
                log::trace!("Drawing lines to child: {}", child);

                stack.push(child);

                let reingold_tilford::Coordinate {
                    x: child_x,
                    y: child_y,
                } = layout[&child.id];

                let child_top = child_y - Tree.dimensions(child).top;

                cairo.move_to(child_x, child_top);
                cairo.line_to(child_x, meet_point);
                cairo.line_to(x, meet_point);
                cairo.line_to(x, box_bottom);
            }

            cairo
                .stroke()
                .expect("TODO: Can I bubble Cairo errors up sensibly?");
        }
    }
}

#[derive(Debug, Default)]
pub struct WorkspacePage {
    pub containers: std::rc::Rc<std::cell::RefCell<Container>>,
    pub drawing_area: gtk::DrawingArea,
}

#[glib::object_subclass]
impl ObjectSubclass for WorkspacePage {
    type ParentType = gtk::Widget;
    type Type = crate::workspace_page::WorkspacePage;

    const NAME: &'static str = "I3TreeVisualWorkspacePageWorkspacePage";
}

impl ObjectImpl for WorkspacePage {
    fn constructed(&self, obj: &Self::Type) {
        log::trace!("Constructing workspace: {:?}", self);

        self.parent_constructed(obj);

        let tree = self.containers.clone();
        self.drawing_area.set_draw_func(
            glib::clone!(@strong tree => @default-panic, move |area, cairo, width, height| {
                tree.borrow()
                    .draw_function(area, cairo, width.into(), height.into());
            }),
        );
    }
}

impl WidgetImpl for WorkspacePage {}
