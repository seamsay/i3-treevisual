//! The implementation of a `struct` to facilitate displaying containers.
//!
//! `DisplayContainer` stores the important information of a _single_ container
//! (so no children, for example), and implements the `Display` trait so that
//! information can be shown properly.

#[derive(Clone, Copy, Debug)]
pub enum Layout {
    Unknown,
    Horizontal,
    Stacking,
    Tabbed,
    Vertical,
}

impl From<i3_ipc::reply::NodeLayout> for Layout {
    fn from(layout: i3_ipc::reply::NodeLayout) -> Self {
        use i3_ipc::reply::NodeLayout::*;
        use Layout::*;

        match layout {
            SplitH => Horizontal,
            SplitV => Vertical,
            Stacked => Stacking,
            i3_ipc::reply::NodeLayout::Tabbed => Layout::Tabbed,
            _ => Unknown,
        }
    }
}

#[derive(Debug)]
pub struct DisplayContainer<'n> {
    pub focused: bool,
    pub id: usize,
    pub layout: Layout,
    pub name: Option<&'n str>,
    pub window: Option<u32>,
}

impl<'n, 'i: 'n> From<&'i i3_ipc::reply::Node> for DisplayContainer<'n> {
    fn from(node: &'i i3_ipc::reply::Node) -> Self {
        DisplayContainer {
            focused: node.focused,
            id: node.id,
            layout: node.layout.into(),
            name: node.name.as_ref().map(|s| &s[..]),
            window: node.window,
        }
    }
}

impl<'n> std::fmt::Display for DisplayContainer<'n> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} {}", self.layout, self.id)?;

        if self.focused {
            write!(f, " 👁")?;
        }

        if let Some(window) = self.window {
            write!(f, " ({})", window)?;
        }

        if let Some(ref name) = self.name {
            write!(f, " [{}]", name)?;
        }

        Ok(())
    }
}

pub trait DisplayContainerisable<'s> {
    fn display<'d>(&'s self) -> DisplayContainer<'d>
    where
        's: 'd;
}

impl<'n> DisplayContainerisable<'n> for i3_ipc::reply::Node {
    fn display<'d>(&'n self) -> DisplayContainer<'d>
    where
        'n: 'd,
    {
        DisplayContainer::from(self)
    }
}
