use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

#[derive(Default)]
pub struct TreeVisual {
    pub outputs:
        std::cell::RefCell<std::collections::HashMap<String, crate::output_window::OutputWindow>>,
    pub title: std::cell::RefCell<Option<String>>,
}

#[glib::object_subclass]
impl ObjectSubclass for TreeVisual {
    type ParentType = gtk::Application;
    type Type = super::TreeVisual;

    const NAME: &'static str = "I3TreeVisualTreeVisualTreeVisual";
}

impl ApplicationImpl for TreeVisual {}

impl GtkApplicationImpl for TreeVisual {}

impl ObjectImpl for TreeVisual {
    fn signals() -> &'static [glib::subclass::Signal] {
        static SIGNALS: once_cell::sync::Lazy<Vec<glib::subclass::Signal>> =
            once_cell::sync::Lazy::new(|| {
                vec![glib::subclass::Signal::builder(
                    "tree-updated",
                    &[],
                    <()>::static_type().into(),
                )
                .build()]
            });

        SIGNALS.as_ref()
    }

    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: once_cell::sync::Lazy<Vec<glib::ParamSpec>> =
            once_cell::sync::Lazy::new(|| {
                vec![glib::ParamSpecString::builder("title")
                    .blurb("Extra information to add to the window's title.")
                    .build()]
            });

        PROPERTIES.as_ref()
    }

    fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "title" => self.title.borrow().to_value(),
            _ => unreachable!(),
        }
    }

    fn set_property(
        &self,
        _obj: &Self::Type,
        _id: usize,
        value: &glib::Value,
        pspec: &glib::ParamSpec,
    ) {
        match pspec.name() {
            "title" => {
                let updated = value.get().expect("I think GTK handles this stuff.");
                *self.title.borrow_mut() = Some(updated);
            }
            _ => unreachable!(),
        }
    }
}
