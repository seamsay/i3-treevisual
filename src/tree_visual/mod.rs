use color_eyre::eyre::{self, ContextCompat};
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};
use i3_ipc::Connect;

use crate::display_container::DisplayContainerisable;
use crate::notify::NotifyResult;

mod implementation;

glib::wrapper! {
    pub struct TreeVisual(ObjectSubclass<implementation::TreeVisual>)
        @extends gtk::Application,
        @implements gio::ActionGroup, gio::ActionMap, gio::Application, gtk::Buildable;
}

impl TreeVisual {
    pub fn new(id: &str) -> eyre::Result<Self> {
        log::debug!("Creating new application with id: {}", id);

        let object: TreeVisual = glib::Object::new(&[("application-id", &id)])?;

        Ok(object)
    }

    pub fn update(&self, tree: &i3_ipc::reply::Node) -> eyre::Result<()> {
        log::debug!("Updating tree: {}", tree.display());

        let mut outputs = self.imp().outputs.borrow_mut();
        let mut new_outputs = std::collections::HashMap::new();

        for node in &tree.nodes {
            if node.name.as_deref() == Some("__i3") {
                continue;
            }

            let name = node.name.clone().wrap_err_with(|| {
                format!("Name expected on output container: {}", node.display(),)
            })?;

            let output = if let Some(output) = outputs.remove(&name) {
                output
            } else {
                let title = {
                    let mut title = String::new();

                    title.push_str(crate::APPLICATION);
                    title.push_str(" - ");

                    if let Some(extra) = self.property::<Option<String>>("title") {
                        title.push_str(&extra);
                        title.push_str(" - ")
                    }

                    title.push_str("Output: ");
                    title.push_str(&name);

                    title
                };

                let output = crate::output_window::OutputWindow::new(self, &title)?;

                output.show();

                output
            };
            output.update(node)?;

            new_outputs.insert(name, output);
        }

        for window in outputs.values() {
            log::info!(
                "Closing window for non-existent output: {}",
                window
                    .title()
                    .expect("Output windows should always be created with titles."),
            );
            window.close();
        }

        *outputs = new_outputs;

        Ok(())
    }
}

pub fn initialise(app: &TreeVisual) {
    let i3 = std::rc::Rc::new(std::cell::RefCell::new(
        i3_ipc::I3::connect().expect("No way to recover from being unable to connect."),
    ));

    app.connect_local(
        "tree-updated",
        false,
        glib::clone!(@strong i3, @weak app => @default-panic, move |_| {
            let tree = i3
                .borrow_mut()
                .get_tree()
                .map_err(eyre::Report::from)
                .unwrap_notify(&app);

            app.update(&tree).unwrap_notify(&app);

            None
        }),
    );

    // Make sure the tree gets drawn straight away.
    app.emit_by_name::<()>("tree-updated", &[]);
}
