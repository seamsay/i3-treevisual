use color_eyre::eyre;
use gtk::glib;
use gtk::prelude::*;

use crate::notify::NotifyResult;

mod display_container;
mod listener;
mod notify;
mod output_window;
mod tree_visual;
mod workspace_page;

fn coloured_log_format(
    w: &mut dyn std::io::Write,
    now: &mut flexi_logger::DeferredNow,
    record: &flexi_logger::Record,
) -> Result<(), std::io::Error> {
    let level = record.level();
    // TODO: Make the level fixed width (`:5` doesn't work).
    write!(
        w,
        "[{}] {} [{}] {}",
        flexi_logger::style(level)
            .paint(now.format(flexi_logger::TS_DASHES_BLANK_COLONS_DOT_BLANK)),
        flexi_logger::style(level).paint(record.level().to_string()),
        record.module_path().unwrap_or("<unnamed>"),
        flexi_logger::style(level).paint(&record.args().to_string()),
    )
}

fn log_format(
    w: &mut dyn std::io::Write,
    now: &mut flexi_logger::DeferredNow,
    record: &flexi_logger::Record,
) -> Result<(), std::io::Error> {
    write!(
        w,
        "[{}] {} [{}] {}",
        now.format(flexi_logger::TS_DASHES_BLANK_COLONS_DOT_BLANK),
        record.level(),
        record.module_path().unwrap_or("<unnamed>"),
        &record.args(),
    )
}

const QUALIFIER: &str = "xyz";
const ORGANISATION: &str = "seamsay";
const APPLICATION: &str = env!("CARGO_PKG_NAME");

fn main() -> eyre::Result<()> {
    color_eyre::install()?;

    // This is needed so that we can match on class in i3.
    glib::set_program_name(Some("i3-treevisual"));

    let app =
        tree_visual::TreeVisual::new(&format!("{}.{}.{}", QUALIFIER, ORGANISATION, APPLICATION))?;

    app.add_main_option(
        "log-level",
        'l'.try_into().expect("Hardcoded character is valid ASCII."),
        glib::OptionFlags::NONE,
        glib::OptionArg::String,
        "Set the minimum level of logs to print.",
        Some("[trace, debug, info, warn, error]"),
    );
    app.add_main_option(
        "log-directory",
        'd'.try_into().expect("Hardcoded character is valid ASCII."),
        glib::OptionFlags::NONE,
        glib::OptionArg::Filename,
        "Directory in which to create log files.",
        Some("<a writeable directory>"),
    );
    app.add_main_option(
        "title",
        't'.try_into().expect("Hardcoded character is valid ASCII."),
        glib::OptionFlags::NONE,
        glib::OptionArg::String,
        "An extra string to add to each window's title.",
        None,
    );

    app.connect_handle_local_options(move |app, options| {
        if let Some(title) = options
            .lookup::<String>("title")
            .expect("GTK should have ensured this is the correct type.")
        {
            app.set_property("title", &title);
        }

        let log_directory = options
            .lookup::<std::path::PathBuf>("log-directory")
            .expect("GTK should have ensured this is the correct type.")
            .unwrap_or_else(|| {
                directories::ProjectDirs::from(QUALIFIER, ORGANISATION, APPLICATION)
                    .expect("A setup where this returns `None` would not be able to run i3.")
                    .state_dir()
                    .expect("This directory always exists on Linux.")
                    .into()
            });

        let log_level = options
            .lookup::<String>("log-level")
            .expect("GTK should have ensured this is the correct type.");
        let log_level = log_level.as_deref().unwrap_or("info");

        flexi_logger::Logger::try_with_env_or_str(log_level)
            .unwrap_notify(app)
            .adaptive_format_for_stderr(flexi_logger::AdaptiveFormat::Custom(
                log_format,
                coloured_log_format,
            ))
            .format_for_files(log_format)
            .log_to_file(flexi_logger::FileSpec::default().directory(&log_directory))
            .duplicate_to_stderr(flexi_logger::Duplicate::All)
            .start()
            .unwrap_notify(app);

        glib::log_set_default_handler(glib::rust_log_handler);

        log::info!("Log directory: {}", log_directory.display());

        -1
    });

    app.connect_startup(listener::start);
    app.connect_activate(tree_visual::initialise);

    std::process::exit(app.run());
}
