use color_eyre::eyre;
use gtk::glib;
use gtk::prelude::*;

use crate::display_container::DisplayContainerisable;
use crate::notify::NotifyResult;

enum Changed {
    Output,
    Workspace,
    Window,
    Exec,
    Unknown,
}

pub fn start(app: &crate::tree_visual::TreeVisual) {
    let mut i3_events = i3_ipc::I3Stream::conn_sub(&[
        i3_ipc::event::Subscribe::Workspace,
        i3_ipc::event::Subscribe::Output,
        i3_ipc::event::Subscribe::Window,
        i3_ipc::event::Subscribe::Binding,
    ])
    .expect("No way to recover from being unable to connect.");

    // TODO: If we use async then we won't need the channel.
    let (transmitter, receiver) = gtk::glib::MainContext::channel(gtk::glib::PRIORITY_DEFAULT);

    std::thread::spawn(move || {
        use i3_ipc::event::Event::*;

        let transmitter = transmitter.clone();

        for event in i3_events.listen() {
            let send = event.and_then(|event| match event {
                Workspace(data) => {
                    log::debug!(
                        "Received workspace event: {:?} {:?} -> {:?}",
                        data.change,
                        data.old
                            .as_ref()
                            .map(crate::display_container::DisplayContainer::from),
                        data.current
                            .as_ref()
                            .map(crate::display_container::DisplayContainer::from)
                    );
                    Ok(Some(Changed::Workspace))
                }
                Output(data) => {
                    log::debug!("Received output event: {}", data.change);
                    Ok(Some(Changed::Output))
                }
                Window(data) => {
                    log::debug!(
                        "Received window event: {:?} {}",
                        data.change,
                        &data.container.display(),
                    );
                    Ok(Some(Changed::Window))
                }
                // `exec` is needed because you could execute a script which runs one of the other
                // commands.
                Binding(data) if data.binding.command.starts_with("exec") => {
                    log::debug!(
                        "Received exec binding: {} {}",
                        data.change,
                        data.binding.command
                    );
                    Ok(Some(Changed::Exec))
                }
                // `focus` is needed because `focus parent` won't trigger a `window` event. `layout`
                // is needed because it doesn't trigger a `window` event.
                Binding(data)
                    if data.binding.command.starts_with("focus")
                        || data.binding.command.starts_with("layout") =>
                {
                    log::debug!(
                        "Received binding event: {} {}",
                        data.change,
                        data.binding.command,
                    );
                    Ok(Some(Changed::Unknown))
                }
                Binding(data) => {
                    log::info!("Ignoring binding with command: {}", data.binding.command);
                    Ok(None)
                }
                event => unreachable!("Did not subscribe to event: {:?}", event),
            });

            transmitter
                .send(send)
                .expect("No way to recover from being unable to send a message.");
        }
    });

    receiver.attach(
        None,
        glib::clone!(@weak app => @default-panic, move |message| {
            message
                .map(|changed| {
                    if changed.is_some() {
                        app.emit_by_name::<()>("tree-updated", &[]);
                    }
                })
                .map_err(eyre::Report::from)
                .unwrap_notify(&app);

            glib::Continue(true)
        }),
    );
}
